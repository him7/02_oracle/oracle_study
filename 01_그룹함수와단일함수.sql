-------------------------
-- 연습문제
-- 1. EMPLOYEE테이블에 이름, 입사일 조회
--    입사일에 포멧 적용한다.
--    '2018년 6월 15일 (수)' 형식으로 출력 처리하세요.

SELECT
	EMP_NAME AS "이름",
	HIRE_DATE AS "입사일",
	TO_CHAR(HIRE_DATE, 'YYYY"년" fmMM"월" DD"일" "("DY")"') AS "입사일 포맷"
FROM
	EMPLOYEE e ;

-- 2. 직원명과 주민번호를 조회함
--  단, 주민번호 9번째 자리부터 끝까지는 '*'문자로 채움
--  예 : 홍길동 771120-1******

SELECT
	EMP_NAME AS "직업명",
	RPAD(SUBSTR(EMP_NO, 0, 8), 14, '*') AS "주민번호"
FROM
	EMPLOYEE e ;

-- 3. 직원명, 직급코드, 연봉(원) 조회
--    단, 연봉은 ￦57,000,000 으로 표시되게 함
--    연봉은 보너스포인트가 적용된 1년치 급여임

SELECT
	EMP_NAME AS "직원명",
	JOB_CODE AS "직급코드",
	TO_CHAR((SALARY * 12) + (SALARY * NVL(BONUS, 0) ), 'L99,999,999') AS "연봉(원)"
FROM
	EMPLOYEE e ;

-- 4. 부서코드가 D5, D9인 직원들 중에서 2004년도에 입사한 직원의 수를 출력하세요.

SELECT
	COUNT(*)
FROM
	EMPLOYEE e
WHERE
	1 = 1
	AND DEPT_CODE IN ('D5', 'D9')
	AND EXTRACT(YEAR FROM HIRE_DATE) = 2004;

-- 5. 직원명, 부서코드, 생년월일, 나이(만) 조회
--    단, 생년월일은 주민번호에서 추출해서, 
--    ㅇㅇ년 ㅇㅇ월 ㅇㅇ일로 출력되게 함.
--    나이는 주민번호에서 추출해서 날짜데이터로 변환한 다음, 계산함

SELECT
	EMP_NAME AS "직원명",
	DEPT_CODE AS "부서코드",
	TO_CHAR(TO_DATE(SUBSTR(EMP_NO, 1, 6), 'RRMMDD'), 'RR"년 "MM"월 "DD"일"') AS "생년월일",
	FLOOR((SYSDATE - TO_DATE(SUBSTR(EMP_NO, 1, 6), 'RRMMDD'))/365) AS "나이(만)"
FROM
	EMPLOYEE e
WHERE
	1 = 1
	AND SUBSTR(EMP_NO, 3, 2) <= 12
	AND SUBSTR(EMP_NO, 5, 2) <= 31  
